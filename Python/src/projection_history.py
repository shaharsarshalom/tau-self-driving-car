import rosbag
import os

from ros_numpy.image import image_to_numpy
from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array, pointcloud2_to_array
import cv2
from scipy.io import savemat

###
frame_num = 0
    # while video is running
    # get the path/directory
    folder_dir = '/home/user/Desktop/Diana/images'
    lst = os.listdir(folder_dir)
    lst.sort()
    for image1 in lst:
        frame = cv2.imread(os.path.join(folder_dir, image1))

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(frame)

        frame_num += 1
        print('Frame #: ', frame_num)
        frame_size = frame.shape[:2]
        image_data = cv2.resize(frame, (input_size, input_size))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        start_time = time.time()




#####


# Rotation
R = roty(self.ry)  # [3, 3]
points_3d = R @ box_coord

# Translation
points_3d[0, :] = points_3d[0, :] + self.t[0]
points_3d[1, :] = points_3d[1, :] + self.t[1]
points_3d[2, :] = points_3d[2, :] + self.t[2]

if is_homogenous:
    points_3d = np.vstack((points_3d, np.ones(points_3d.shape[1])))

return points_3d


###
from matplotlib import image

from matplotlib import pyplot
import numpy as np
from PIL import Image

from numpy import asarray

# load image as array
# what is the diff between the options ?
#op1
image = image.imread('')
#op2
image = Image.open('')
data = asarray(image)
#op3
im = np.array(Image.open('').convert('L'))

# load lidar point as array
lidar = np.fromfile(velo_filename, dtype=np.float32)
lidar = lidar.reshape((-1, 3))
lidar=lidar.T

#rotation matrix - MISSING
R = np.array(
    [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]])

# intrinsic matrix
intrinsic= np.array(
    [[880.9275, 0, 0],
     [0, 883.9441, 0],
     [908.6900, 613.2250, 1]])

#translation vector - MISSING
t=np.array(
    [[0], [0], [0]])

## we don't need this any more
#load camera calibration mat
#Read in a calibration file and parse into a dictionary

def read_calib_file(filepath):
    data = {}

    with open(filepath, 'r') as f:
        for line in f.readlines():
            key, value = line.split(':', 1)
            # The only non-float values in these files are dates, which
            # we don't care about anyway
            try:
                data[key] = np.array([float(x) for x in value.split()])
            except ValueError:
                pass

    return data


#Transforation matrix from rotation matrix and translation vector
def transform_from_rot_trans(R, t):
    R = R.reshape(3, 3)
    t = t.reshape(3, 1)
    return np.vstack((np.hstack([R, t]), [0, 0, 0, 1]))

translation_mat= transform_from_rot_trans(R, t)
###
#translation_mat =  R   |    t
#                  0 , 0 ,0 , 0, 1

## multiply matrix :
## result = [4x4] * ([nx4]^T)
## result = [4x4] * [4xn] = [4xn]
## result = [ [u] ,[v] , [z??] ,[1]]
result = np.dot(translation_mat,lidar)

##
###
#new option
###

## 3D -> 3D velo to camera
#velodyne_points -> [nx3] need to add 1 and ^T
#translation_mat -> [4x4]
R = np.array(
    [[-0.0008, -0.0366, 0.9993],
     [-0.9998, -0.0188, -0.0015],
     [0.0188, -0.9992, -0.0366]])
t=np.array(
    [[0.0410], [-0.1038], [-0.2343]])
def transform_from_rot_trans(R, t):
    R = R.reshape(3, 3)
    t = t.reshape(3, 1)
    return np.vstack((np.hstack([R, t]), [0, 0, 0, 1]))

translation_mat= transform_from_rot_trans(R, t)
num_of_lines = len(velodyne_points)
ones=np.ones(num_of_lines,dtype=int)
lidar = velodyne_points.T
lidar = np.vstack((lidar,ones))
result3D = np.dot(translation_mat,lidar)

##  store z values in camera coordinates
z_value= result3D[2:3,:]

## 3D -> 2D  camera to image
intrinsic= np.array(
    [[880.9275, 0, 0],
     [0, 883.9441, 0],
     [908.6900, 613.2250, 1]])

result2D = np.dot(intrinsic.T,(result3D[:3,:]))


## include z values in matrix :

result2D_with_z=np.vstack(result2D[:2,:],z_value)


## project on image :
#################################################################################
## THIS WORKS : ##
#################################################################################

import numpy
import rosbag
import os

#from ros_numpy.image import image_to_numpy
#from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array, pointcloud2_to_array
import cv2
from scipy.io import savemat
from matplotlib import image, pyplot as plt
from matplotlib import pyplot

import numpy as np
from PIL import Image

from numpy import asarray
import numpy as np
import scipy.io
import sys
import numpy as np


import h5py
###
#new option
###

## 3D -> 3D velo to camera
#velodyne_points -> [nx3] need to add 1 and ^T
#translation_mat -> [4x4]
#R = np.array(
#    [[-0.0008, -0.0366, 0.9993],
#     [-0.9998, -0.0188, -0.0015],
#     [0.0188, -0.9992, -0.0366]])

R = np.array([[-0.000830795488850,-0.036645123217335, 0.999327996567314],
  [-0.999822733583387 , -0.018766802899451 , -0.001519381050638],
   [0.018809869449297 , -0.999152111569264 , -0.036623035892467]])


#t=np.array(
#    [[0.0410], [-0.1038], [-0.2343]])
t = np.array([[0.040999832514810], [-0.103757543960500], [-0.234292099334300]])


def transform_from_rot_trans(R, t):
    R = R.reshape(3, 3)
    t = t.reshape(3, 1)
    return np.vstack((np.hstack([R, t]), [0, 0, 0, 1]))

##
mat = scipy.io.loadmat('/home/user/Desktop/Diana/pointcloud/1.mat')
camera_model_mat_file = scipy.io.loadmat('/home/user/Documents/tau-self-driving-car/lidar_to_camera_trans.mat')
print(camera_model_mat_file)
camera_model_mat = scipy.io.loadmat('/home/user/Documents/tau-self-driving-car/cameraParams_2022_02_10_large_board.mat')

velodyne_points = np.array(mat['velodyne_points']) # For converting to a NumPy array

#'/home/user/Desktop/ROS/test4_cam_and_lidar_2022-02-10-16-06-15.bag'
#velodyne_points = numpy.loadtxt(velo_filename, dtype=np.float64)
#velodyne_points = velodyne_points.reshape((-1,3))

translation_mat= transform_from_rot_trans(R.T, t)
#translation_mat.shape
#print(translation_mat)


num_of_lines = len(velodyne_points)
ones=np.ones(num_of_lines,dtype=int)
lidar = velodyne_points.T
lidar.shape
lidar = np.vstack((lidar,ones))
lidar.shape
#lidar_points_to_camera_3d = np.dot(translation_mat, lidar)
lidar_points_to_camera_3d = np.matmul(translation_mat, lidar)
lidar_points_to_camera_3d.shape

if False:
    lidar_points_to_camera_3d.shape

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(lidar_points_to_camera_3d[0,:], lidar_points_to_camera_3d[1,:], lidar_points_to_camera_3d[2,:])
    ax.scatter(lidar[0, :], lidar[1, :], lidar[2, :])
    plt.show()

##  store z values in camera coordinates
z_value = lidar_points_to_camera_3d[2:3, :]

## 3D -> 2D  camera to image
# CAMERA CALIBRATION - FIRST TRY
intrinsic= np.array(
    [[880.9275, 0, 0],
     [0, 883.9441, 0],
     [908.6900, 613.2250, 1]])

# LARGE BOARD
intrinsic= 1e3 * np.array([[1.039641431856563,0,0], [0,1.036702725659338,0], [0.954962738197960, 0.601870249376522, 0.001000000000000]])

lidar_points_on_image_plane = np.matmul(intrinsic.T, (lidar_points_to_camera_3d[:3, :]))
#result2D = np.dot(intrinsic.T, (lidar_points_to_camera_3d[:3, :]))

lidar_points_on_image_plane.shape
ind_inlier = np.where(lidar_points_on_image_plane[-1,:] >= 0)
lidar_points_on_image_plane = lidar_points_on_image_plane[:,ind_inlier[0]]
lidar_points_on_image_plane.shape

lidar_points_on_image_plane_org = lidar_points_on_image_plane.copy()
lidar_points_on_image_plane[0, :] = lidar_points_on_image_plane[0, :] / lidar_points_on_image_plane[-1, :]
lidar_points_on_image_plane[1, :] = lidar_points_on_image_plane[1, :] / lidar_points_on_image_plane[-1, :]
lidar_points_on_image_plane[-1, :] = lidar_points_on_image_plane[-1, :] / lidar_points_on_image_plane[-1, :]

if False:
    ## project on image :
    import cv2
    image = cv2.imread('/home/user/Desktop/Diana/images/1.png')
    type(image)
    image.shape
    plt.figure()
    plt.scatter(lidar_points_on_image_plane[0, :], lidar_points_on_image_plane[1, :], marker=".", color="red", s=5)
    plt.imshow(image)
    plt.show()

xs = 750
ys = 600
ind=np.where(np.logical_and(np.logical_and(lidar_points_on_image_plane[0, :] > (xs - 10), lidar_points_on_image_plane[0, :] < (xs + 10)),
                            np.logical_and(lidar_points_on_image_plane[1, :] > (ys - 50), lidar_points_on_image_plane[1, :] < (ys + 50))))
np.median(lidar_points_on_image_plane_org[2, ind[0]])

xs = 1500
ys = 600
ind=np.where(np.logical_and(np.logical_and(lidar_points_on_image_plane[0, :] > (xs - 10), lidar_points_on_image_plane[0, :] < (xs + 10)),
                            np.logical_and(lidar_points_on_image_plane[1, :] > (ys - 50), lidar_points_on_image_plane[1, :] < (ys + 50))))
print(np.median(lidar_points_on_image_plane_org[2, ind[0]]))

xs = 1805
ys = 657
ind=np.where(np.logical_and(np.logical_and(lidar_points_on_image_plane[0, :] > (xs - 10), lidar_points_on_image_plane[0, :] < (xs + 10)),
                            np.logical_and(lidar_points_on_image_plane[1, :] > (ys - 50), lidar_points_on_image_plane[1, :] < (ys + 50))))
print(np.median(lidar_points_on_image_plane_org[2, ind[0]]))



## include z values in matrix :

result2D_with_z=np.vstack((lidar_points_on_image_plane[:2, :], z_value))




























