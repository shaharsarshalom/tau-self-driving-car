
#######
import math

import numpy as np
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from filterpy.kalman import KalmanFilter

if 0:
    mu, sigma = 0, 0.1 # mean and standard deviation

    # Get x values of the sine wave
    time = np.arange(0, 10, 0.1)
    # Amplitude of the sine wave is sine of a variable like time
    amplitude = np.sin(time)

    s = np.random.normal(mu, sigma, len(time))
    amplitude_noise = amplitude + s

    # Plot a sine wave using time and amplitude obtained for the sine wave
    fig = plt.figure()
    plt.plot(time, amplitude, '*')
    plt.plot(time, amplitude_noise, '*')
    plt.grid(True)
    plt.draw()


    #####################

    # pip install filterpy

    f = KalmanFilter (dim_x=2, dim_z=1)

    # f.x = np.array([[2.],    # position
    #                 [0.]])   # velocity

    f.x = np.array([0, 0])

    f.F = np.array([[1.,1.],
                    [0.,1.]])

    f.H = np.array([[1.,0.]])

    #f.P *= 1000.
    f.P = np.array([[sigma*sigma,    0.],
                    [   0., 1] ])

    f.R = np.array([[0.01]])

    from filterpy.common import Q_discrete_white_noise
    f.Q = Q_discrete_white_noise(dim=2, dt=0.1, var=0.13)

    predicated_vec = []
    for current_amplitude_noise in amplitude_noise:
        f.predict()
        predicated_state = f.x
        predicated_vec.append(predicated_state)
        f.update(current_amplitude_noise)
        print(predicated_state, current_amplitude_noise)

    # Plot a sine wave using time and amplitude obtained for the sine wave
    fig = plt.figure()
    plt.plot(time, amplitude, '*')
    plt.plot(time, amplitude_noise, '*')
    pred_loc = [f[0] for f in predicated_vec]
    plt.plot(time, pred_loc, '*')
    plt.legend()

    np.mean(np.abs(pred_loc - amplitude))
    np.mean(np.abs(amplitude_noise - amplitude))

################ 2d kalman filter

mu = 0
sigma_x = 0.5
sigma_y = 0.1
# Get x values of the sine wave
import math
time = np.arange(0, 2*math.pi, 0.1)
# Amplitude of the sine wave is sine of a variable like time
xs = 2*np.sin(time)
ys = 4*np.cos(time)
xs.shape
xs_noise = xs + np.random.normal(mu, sigma_x, len(time))
ys_noise = ys + np.random.normal(mu, sigma_y, len(time))

if False:
    # Plot a sine wave using time and amplitude obtained for the sine wave
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(time, xs, ys)
    plt.grid(True)
    plt.draw()

if False:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #plt.plot(ys, xs, '*')
    plt.plot(xs, ys, '*')
    plt.plot(xs_noise, ys_noise, '*')
    ax.set_aspect('equal', adjustable='box')
    plt.grid(True)
    plt.draw()

#plt.plot(time, amplitude_noise, '*')
from filterpy.kalman import KalmanFilter
f = KalmanFilter(dim_x=4, dim_z=2)

# initial state
f.x.shape
f.x = np.array([[xs[0]], [ys[0]], [0], [0]])

f.F.shape
f.F = np.array([[1.,0, 1., 0],
                [0.,1., 0, 1.],
                [0.,0., 1.0, 0],
                [0.,0., 0, 1.]])

f.H.shape
#f.H = np.array([[1.,1., 0., 0.]])
f.H = np.array([[1.,0., 1., 0.], [0.,1., 0., 1.]])


#f.P *= 1000.
f.P = np.array([[sigma_x*sigma_x,    0., 0., 0.],
                [0,    sigma_y*sigma_y, 0., 0.],
                [0,    0., 1, 0.],
                [0,    0., 0., 1]])

f.P.shape
f.R.shape
#f.R = np.array([[0.01], [0.01]])

from filterpy.common import Q_discrete_white_noise
f.Q.shape
f.Q = Q_discrete_white_noise(dim=4, dt=0.1, var=0.05)

predicated_vec = []
for current_xs_noise, current_ys_noise in zip(xs_noise, ys_noise):
    f.predict()
    predicated_state = f.x
    predicated_vec.append(predicated_state)
    tt = np.array([current_xs_noise, current_ys_noise])
    tt = np.expand_dims(tt,axis=1)
    #tt.shape
    f.update(tt)
    print(predicated_state)


fig = plt.figure()
ax = fig.add_subplot(111)
#plt.plot(ys, xs, '*')
plt.plot(xs, ys, '*')
plt.plot(xs_noise, ys_noise, '*')
plt.plot([f[0] for f in predicated_vec], [f[1] for f in predicated_vec], '*')

ax.set_aspect('equal', adjustable='box')
plt.grid(True)
plt.draw()
