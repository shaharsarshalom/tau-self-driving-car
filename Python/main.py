#!/usr/bin/env python
# conda create -y -n RosLidar27_1 python=2.7
# conda activate RosLidar27_1
# conda install -c conda-forge ros-rosbag

# sudo apt install ros-noetic-ros-numpy
# follow https://answers.ros.org/question/270439/ros_numpy-package/
# this is magic --> pip3 install pyrosenv
# pip3 install ros-get

# sudo apt-get install ros-noetic-sensor-msgs

# from sensor_msgs.msg import PointCloud2, PointField

from sensor_msgs.msg import PointCloud2

# handle bag and pointcloud2 msg type + numpy version of ros
import numpy as np
import os
import rosbag

from ros_numpy.image import image_to_numpy
from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array, pointcloud2_to_array
import cv2
from scipy.io import savemat
import sys

from datetime import datetime
import rospy

# check for 1 arg
# if len(sys.argv) != 2:
#     exit(-1)
# bagfile name as an argument, for example 'test2.bag'
#bag=rosbag.Bag(sys.argv[1])

# https://github.com/eric-wieser/ros_numpy

#file_name = '/home/gaming/Desktop/2021-11-08-13-18-24.bag'
#file_name = '/media/gaming/28BB-8396/recording/2021-11-09-13-42-23.bag'
# file_name = '/home/gaming/Desktop/2021-11-09-13-42-23.bag'
file_name = '/home/user/Desktop/ROS/calibration_2021-12-26-12-16-36.bag' #path of bag file
directoryI='/home/user/Desktop/Diana/images'  #path of saved images
directoryP='/home/user/Desktop/Diana/pointcloud' #path of saved PC
image_number=0;
pointcloud_number=0;
bag=rosbag.Bag(file_name)

num_messages = 0
keys = dict()
for topic,message,timestamp in bag.read_messages():
    keys[topic] = 1
    num_messages += 1
    print(type(message))

    if topic == '/sensor_sync/velodyne_points':
        found_velodyne = True
        pointcloud_number=pointcloud_number+1;
        velodyne_points_raw = message
        velodyne_points = pointcloud2_to_xyz_array(message)
        os.chdir(directoryP)
        filenameP = str(pointcloud_number)+ '.mat'
        savemat(filenameP, {'velodyne_points': velodyne_points})

        #os.chdir(directoryP)
        #filenameP = str(timestamp)
        #savemat(filenameP, velodyne_points)
        #scipy.io.savemat('test.mat', {'mydata': mydata})
        #savemat(filenameP, {'velodyne_points':velodyne_points})

        if False:
            os.chdir(directoryP)
            filenameP=str(timestamp)
            savemat(filenameP,velodyne_points)
        # this also can be useful
        # velodyne_points = pointcloud2_to_array(message)

        if False:
            type(message)
            import ros_numpy
            from ros_numpy import rosbag
            #import ros_numpy import numpy
            import ros_numpy
            from sensor_msgs.msg import PointCloud2, PointField, Image
            from ros_numpy import numpy_msg

            from rospy.numpy_msg import numpy_msg
            from rospy.numpy_msg import numpy_msg

            numpy_msg.read
            from sensor_msgs.msg import Image

            numpify

            from ros_numpy.registry import numpify
            new_points_arr = numpify(message)

            new_points_arr = ros_numpy.numpify(message)

            import ros_numpy as rnp  # if missing, to install via shell: sudo apt install ros-melodic-ros-numpy
            lidar_arr = rnp.pointcloud2_to_xyz_array(message)  # ,remove_nans=True)
            lidar_arr = rnp.point_cloud2.pointcloud2_to_xyz_array(message)  # ,remove_nans=True)
            time_date = datetime.fromtimestamp(timestamp.to_time())

            # [V]
            from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array, pointcloud2_to_array
            lidar_arr = pointcloud2_to_xyz_array(message)
            # [V]
            temp = pointcloud2_to_array(message)


    elif topic == '/sensor_sync/image_raw':
        found_img = True
        image_number=image_number+1;
        image_raw = message

        img_bayer = image_to_numpy(message)
        img_rgb = cv2.cvtColor(img_bayer, cv2.COLOR_BAYER_RG2RGB, 0)
        os.chdir(directoryI)
        filenameI = str(image_number) + '.png'
        cv2.imwrite(filenameI, img_rgb)

        if False:
            os.chdir(directoryI)
            filenameI=str(timestamp)+'.png'
            cv2.imwrite(filenameI,img_rgb)

        if False:
            #[V]
            from ros_numpy.image import image_to_numpy
            img = image_to_numpy(message)
            import cv2
            temp = cv2.cvtColor(img, cv2.COLOR_BAYER_RG2RGB, 0)
            temp.shape
            img.shape

            # this doesn't work
            #import ros_numpy
            #from sensor_msgs.msg import PointCloud2, PointField, Image
            #message_numpy = ros_numpy.numpify(message)

            type(message)
            type(message.data)
            num_elements_bytes = len(message.data)
            num_elements = message.height * message.width

            data_numpy = np.frombuffer(message.data, dtype=np.uint8)
            img = np.reshape(data_numpy, (message.height, message.width))

    elif topic == '/Inertial_Labs/sensor_data':
        # you cannot convert the message to numpy because its conversion is not supported
        found_sensor_data = True
        sensor_data = message

    elif topic == '/Inertial_Labs/ins_data':
        # you cannot convert the message to numpy because its conversion is not supported
        found_sensor_data = True
        ins_data = message

        # this doesn't work
        #from ros_numpy.registry import converts_to_numpy, numpify
        #numpify(message)

    elif topic == '/Inertial_Labs/gps_data':
        # you cannot convert the message to numpy because its conversion is not supported
        found_sensor_data = True
        gps_data = message



stop = 1
if False:
    # printing in a loop over all the msgs in the bag file, that are related to the lidar points cloud.
    for topic,message,timestamp in bag.read_messages(topics=['/velodyne_points']):

        lidar_arr = rnp.point_cloud2.pointcloud2_to_xyz_array(message) #,remove_nans=True)
        time_date = datetime.fromtimestamp(timestamp.to_time())
        print(time_date)
        print(lidar_arr)
        print(lidar_arr.shape)
        print(type(lidar_arr))