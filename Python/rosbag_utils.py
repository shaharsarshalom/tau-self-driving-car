import numpy as np
import rosbag
import os

from ros_numpy.image import image_to_numpy
from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array, pointcloud2_to_array
import cv2
from scipy.io import savemat
import sys

from datetime import datetime
import rospy

############file_name = '/home/user/Desktop/ROS/calibration_2021-12-26-12-25-33.bag' #path of bag file
directoryI='/home/user/Desktop/Diana/images'  #path of saved images
directoryP='/home/user/Desktop/Diana/pointcloud' #path of saved PC


class RosbagUtility:
    def __init__(self, file_name):
        self.directoryI = '/home/user/Desktop/Diana/images'  # path of saved images
        self.directoryP = '/home/user/Desktop/Diana/pointcloud'  # path of saved PC
        self.bag = rosbag.Bag(file_name)
        self.num_messages = 0
        self.keys = dict()

        self.img_rgb = None
        self.velodyne_points = None

        self.ros_bag_generator = self.bag.read_messages()

        self.all_gps_data = []
        self.all_ins_data = []
        self.all_sensor_data = []

        for self.topic, self.message, self.timestamp in self.bag.read_messages():
            self.keys[self.topic] = 1
            self.num_messages += 1
            print(type(self.message))

        print(self.keys.keys())
        pass

    def iterate_all_data(self):

        is_save = False

        for topic, message, timestamp in self.bag.read_messages():
            self.keys[topic] = 1
            self.num_messages += 1
            print(type(message))

            if topic == '/sensor_sync/velodyne_points':
                found_velodyne = True
                velodyne_points_raw = message
                self.velodyne_points = pointcloud2_to_xyz_array(message)

                if is_save:
                    os.chdir(directoryP)
                    filenameP = str(timestamp)
                    savemat(filenameP, {'velodyne_points': self.velodyne_points})

            elif topic == '/sensor_sync/image_raw':
                found_img = True
                image_raw = message

                img_bayer = image_to_numpy(message)
                self.img_rgb = cv2.cvtColor(img_bayer, cv2.COLOR_BAYER_RG2RGB, 0)

                if is_save:
                    os.chdir(directoryI)
                    filenameI = str(timestamp) + '.png'
                    cv2.imwrite(filenameI, self.img_rgb)

            elif topic == '/Inertial_Labs/sensor_data':
                # you cannot convert the message to numpy because its conversion is not supported
                found_sensor_data = True
                sensor_data = message
                self.all_sensor_data.append(sensor_data)

            elif topic == '/Inertial_Labs/ins_data':
                # you cannot convert the message to numpy because its conversion is not supported
                found_sensor_data = True
                ins_data = message

                # this doesn't work
                # from ros_numpy.registry import converts_to_numpy, numpify
                # numpify(message)
                self.all_ins_data.append(ins_data)

            elif topic == '/Inertial_Labs/gps_data':
                # you cannot convert the message to numpy because its conversion is not supported
                found_sensor_data = True
                gps_data = message
                self.all_gps_data.append(gps_data)

    def get_data(self):
        """
        Get single frame
        :return:
        """

        while True:
            is_save = False
            try:
                topic, message, timestamp = next(self.ros_bag_generator)
            except StopIteration:
                raise StopIteration

        # self.keys[topic] = 1
        # self.num_messages += 1
        # print(type(message))


            if 'velodyne_points' in topic:
                #  == '/sensor_sync/velodyne_points'
                found_velodyne = True
                velodyne_points_raw = message
                self.velodyne_points = pointcloud2_to_xyz_array(message)

                if is_save:
                    os.chdir(directoryP)
                    filenameP = str(timestamp)
                    savemat(filenameP, {'velodyne_points': self.velodyne_points})

            elif 'image_raw' in topic:
                #  == '/sensor_sync/image_raw'
                found_img = True
                image_raw = message

                img_bayer = image_to_numpy(message)
                self.img_rgb = cv2.cvtColor(img_bayer, cv2.COLOR_BAYER_RG2RGB, 0)

                if is_save:
                    os.chdir(directoryI)
                    filenameI = str(timestamp) + '.png'
                    cv2.imwrite(filenameI, self.img_rgb)

                if self.img_rgb is not None and self.velodyne_points is not None:
                    return (self.img_rgb, self.velodyne_points)

            elif 'sensor_data' in topic:
                # == '/Inertial_Labs/sensor_data'
                # you cannot convert the message to numpy because its conversion is not supported
                found_sensor_data = True
                sensor_data = message

            elif 'ins_data' in topic:
                #  == '/Inertial_Labs/ins_data'
                # you cannot convert the message to numpy because its conversion is not supported
                found_sensor_data = True
                ins_data = message

                # this doesn't work
                # from ros_numpy.registry import converts_to_numpy, numpify
                # numpify(message)

            elif 'gps_data' in topic:
                #  == '/Inertial_Labs/gps_data'
                # you cannot convert the message to numpy because its conversion is not supported
                found_sensor_data = True
                gps_data = message

        return None


def plot_driving_path(ros_bag):
    # plot the drive path in UTM [m] coordinates
    # pip install utm
    lat = [xs.LLH.x for xs in ros_bag.all_gps_data]
    long = [xs.LLH.y for xs in ros_bag.all_gps_data]
    import utm
    import numpy as np
    lat = np.array(lat)
    long = np.array(long)
    EASTING, NORTHING, ZONE_NUMBER, ZONE_LETTER = utm.from_latlon(lat, long)

    import matplotlib.pyplot as plt
    fig = plt.figure()
    plt.plot(EASTING, NORTHING)
    plt.show()


if __name__ == '__main__':
    # wrong implementation of iterator

    file_name = '/home/user/Desktop/ROS/calibration_2021-12-26-12-25-33.bag'  # path of bag file
    file_name = '/home/user/Desktop/ROS/calibration_2021-12-26-12-08-11.bag'
    file_name = '/home/user/Desktop/2021-11-09-14-11-23.bag'
    file_name = '/home/user/Desktop/2021-11-09-13-42-23.bag'
    ros_bag = RosbagUtility(file_name=file_name)

    if False:
        while True:
            try:
                (img_rgb, velodyne_points) = ros_bag.get_data()
            except StopIteration:
                break
                pass
    else:
        ros_bag.iterate_all_data()


    finished = True


    if False:
        plot_driving_path(ros_bag=ros_bag)

    exit(0)


