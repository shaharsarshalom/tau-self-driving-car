vid = videoinput('gige', 1, 'BayerRG8');
src = getselectedsource(vid);

vid.FramesPerTrigger = 900;
%vid.FramesPerTrigger = Inf;


src.AcquisitionFrameCount = 65535;

preview(vid);
src.BalanceWhiteAuto = 'Continuous';
src.ExposureAuto = 'Continuous';
src.AcquisitionFrameRateAbs = 30.000300003;
src.DSPSubregionBottom = 1080;
src.DSPSubregionRight = 1920;
vid.ROIPosition = [0 0 1920 1080];

%vid.LoggingMode = 'disk&memory';
vid.LoggingMode = 'disk';

i=0;
filenum=2;

while(i<filenum)
    filename=['C:\Records\MatlabRecords\GigE_Cam\Test_',datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.mp4'];
    diskLogger = VideoWriter(filename, 'MPEG-4');
    vid.DiskLogger = diskLogger;
    start(vid);
    %pause(25);
    stop(vid);
    i=i+1;
end


start(vid);
clock;
%[~,ts]=getdata(vid,300);


%stoppreview(vid);

%stop(vid);