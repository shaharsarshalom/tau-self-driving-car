clc;
clear all; %#ok<CLALL>
RecordTime = 10; % secs
%% control
is_display = false; 
is_velodyne = true; 
is_vis = true;
is_boson = true;

%%
% 
g = gigecam; 
img = snapshot(g);
figure, imshow(img);
frameIdx=1;
while frameIdx<10 
    
       tic 
       
       filename = [sprintf('%04d',frameIdx) '.jpg'];
       fullname = fullfile(CameraFileName,filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
       frameIdx = frameIdx+1;
       
       elapsedTime = toc 
end

%% parameters
FrameRate = 10;

%% Recording setup
WorkingDir = 'C:\Records\MatlabRecords\RecordsDianaAndYuval\';
t = datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF');
WorkingDir = fullfile(WorkingDir, t);
mkdir(WorkingDir);
if is_boson
    boson = videoinput('winvideo', 1, 'I420_640x512');
    srcBoson = getselectedsource(boson);
end
if is_vis
    vid = videoinput('gige', 1, 'BayerRG8');
    srcVid = getselectedsource(vid);
    
    if false 
        frame = getsnapshot(boson);
        figure, imshow(frame(:,:,1), [])
    end
end

%% Open Vision camera 
if is_vis
    vid.FramesPerTrigger = RecordTime*FrameRate; %num of frames 
    srcVid.AcquisitionFrameCount = RecordTime*FrameRate;
    srcVid.BalanceWhiteAuto = 'Continuous';
    srcVid.ExposureAuto = 'Continuous';
    srcVid.AcquisitionFrameRateAbs = FrameRate;
    srcVid.DSPSubregionBottom = 1080;
    srcVid.DSPSubregionRight = 1920;
    vid.ROIPosition = [0 0 1920 1080];
    vid.LoggingMode = 'disk';
    filenameRGB=[WorkingDir,'\RGB_' datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.avi'];
    diskLoggerRGB = VideoWriter(filenameRGB);
    diskLoggerRGB.FrameRate = FrameRate;
    vid.DiskLogger = diskLoggerRGB;
end

%% Open boson camera 
if is_boson
    boson.FramesPerTrigger = FrameRate*RecordTime;
    srcBoson.FrameRate = '9.0000';
    boson.LoggingMode = 'disk';
    filename=[WorkingDir,'\TI_' datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.avi'];
    diskLoggerTI = VideoWriter(filename);
    diskLoggerTI.FrameRate = FrameRate;
    boson.DiskLogger = diskLoggerTI;
end

%% lidar setup 
if is_velodyne
    lidar = velodynelidar('VLP16');
end




%% start rec
if is_velodyne
    start(lidar);
end

if is_boson
    start(rot90(boson));
end

if is_vis
    start(vid);
end

if is_display
    preview(boson);
    preview(vid);
    msgbox('Recording..')
end 



%% reading 500 frames from velodyne than stop
if is_velodyne
    numFrames = RecordTime*FrameRate;
    [frames,timestamps] = read(lidar,numFrames);
end

%% stop recording 
if is_velodyne
    stop(lidar);
end

if is_boson
    stop(boson);
end

if is_vis
    stop(vid);
end


if is_display
    stoppreview(vid);
    stoppreview(boson);
    msgbox('Finished Recording. Saving Data..')
end 

%%
%fps=1/mean(seconds(diff(timestamps)));

%%
%create output folders

SavedVideoRGB = VideoReader(filenameRGB);
SavedVideoIR = VideoReader(filename);

if ~exist([WorkingDir ['images_' datestr(now,'dd-mmm-yyyy-HH-MM')]], 'dir')
    CameraFileName = [WorkingDir ['images_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    BosonFileName = [WorkingDir ['Boson_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    LidarFileName = [WorkingDir ['velodynePoints_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    mkdir(CameraFileName);
    mkdir(BosonFileName);
    mkdir(LidarFileName);
end

%%
%save camera video as frames 
frameIdx = 1;

if is_vis
    while hasFrame(SavedVideoRGB)
       img = readFrame(SavedVideoRGB);
       filename = [sprintf('%04d',frameIdx) '.jpg'];
       fullname = fullfile(CameraFileName,filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
       frameIdx = frameIdx+1;
%        if mod(frameIdx,50) == 0 && frameIdx ~= 0
%             img = readFrame(SavedVideoRGB); % dump mod 50 frame because of frame rate mismatch
%        end

    end
end

%%
%save Boson video as frames
frameIdx = 1;

if is_boson
    while hasFrame(SavedVideoIR)
       img = readFrame(SavedVideoIR);
       filename = [sprintf('%04d',frameIdx) '.jpg'];
       fullname = fullfile(BosonFileName,filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
       frameIdx = frameIdx+1;
%        if mod(frameIdx,50) == 0 && frameIdx ~= 0
%             img = readFrame(SavedVideoIR); % dump mod 50 frame because of frame rate mismatch
%        end

    end
end 

%%
%saving lidar pcap as frames
if is_velodyne
    if frameIdx < numFrames
        numFrames = frameIdx;
    end

    for pcapIdx = 1:numFrames
        frameName = [sprintf('%04d',pcapIdx) '.ply'];
        Path = [LidarFileName '\' frameName];
        pcwrite(frames(pcapIdx),Path);
        %pcshow(frames(pcapIdx))

        if false
            figure, pcshow(frames(pcapIdx))
        end 
    end
end