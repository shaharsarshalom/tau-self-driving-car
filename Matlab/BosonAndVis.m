
clc;
clear all; %#ok<CLALL>
RecordTime = 10; % secs
%% camera setup
WorkingDir = 'C:\Records\MatlabRecords\RecordsAlonAndDan\';
boson = videoinput('winvideo', 1, 'I420_640x512');
srcBoson = getselectedsource(boson);
vid = videoinput('gige', 1, 'BayerRG8');
srcVid = getselectedsource(vid);

boson.FramesPerTrigger = 1;
%% parameters
FrameRate = 7.5000;

vid.FramesPerTrigger = inf;
srcVid.AcquisitionFrameCount = RecordTime*FrameRate;
srcVid.BalanceWhiteAuto = 'Continuous';
srcVid.ExposureAuto = 'Continuous';
srcVid.AcquisitionFrameRateAbs = FrameRate;
srcVid.DSPSubregionBottom = 1080;
srcVid.DSPSubregionRight = 1920;
vid.ROIPosition = [0 0 1920 1080];
vid.LoggingMode = 'disk';

srcBoson.FrameRate = '7.5000';
boson.LoggingMode = 'disk';
filename=['C:\Records\MatlabRecords\RecordsAlonAndDan\',datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.mp4'];
diskLogger = VideoWriter(filename, 'MPEG-4');
diskLogger.FrameRate = FrameRate;
boson.DiskLogger = diskLogger;



%% start rec
start(boson);
preview(boson);
%start(vid);
%preview(vid);

%% reding NumOfFrames
numFrames = RecordTime*FrameRate;
[frames,timestamps] = read(boson,numFrames);
stoppreview(boson);

%% stop recording
stoppreview(boson);
stop(boson);
%stoppreview(vid);
%stop(vid);
msgbox('Finished Recording. Saving Data..')
fps=1/mean(seconds(diff(timestamps)));

%% save data - video as frames, velodyne as point clouds


% create output folders
SavedVideo = VideoReader(filename);
frameIdx = 1;
if ~exist([WorkingDir ['images_' datestr(now,'dd-mmm-yyyy-HH-MM')]], 'dir')
    CameraFileName = [WorkingDir ['images_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    BosonFileName = [WorkingDir ['Boson_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    mkdir(CameraFileName);
    mkdir(BosonFileName);
end
%save camera video as frames 
while hasFrame(SavedVideo)
   img = readFrame(SavedVideo);
   filename = [sprintf('%04d',frameIdx) '.jpg'];
   fullname = fullfile(CameraFileName,filename);
   imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
   frameIdx = frameIdx+1;
   if mod(frameIdx,50) == 0 && frameIdx ~= 0
        img = readFrame(SavedVideo); % dump mod 50 frame because of frame rate mismatch
   end
       
end

%saving Boson pcap as frames

if frameIdx < numFrames
    numFrames = frameIdx;
end

for pcapIdx = 1:numFrames
    frameName = [sprintf('%04d',pcapIdx) '.ply'];
    Path = [BosonFileName '\' frameName];
    pcwrite(frames(pcapIdx),Path);
    %pcshow(frames(pcapIdx))
end