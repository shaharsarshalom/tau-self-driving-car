%%%% Autonomous Car Records Setup %%%%%

clc;
clear all; %#ok<CLALL>
RecordTime = 10; % secs

%% camera setup
WorkingDir = ['C:\Records\MatlabRecords\RecordsDianaAndYuval\'];
vid = videoinput('gige', 1, 'BayerRG8');
src = getselectedsource(vid);
%%
boson = videoinput('winvideo', 1, 'I420_640x512');
srcBoson = getselectedsource(boson);
boson.FramesPerTrigger = 1;

%% parameters 
FrameRate = 10.00000; 
maxRate = floor(src.AcquisitionFrameRateLimit);
if maxRate < FrameRate
    FrameRate = maxRate;
end
vid.FramesPerTrigger = inf;
src.AcquisitionFrameCount = RecordTime*10;
src.BalanceWhiteAuto = 'Continuous';
src.ExposureAuto = 'Continuous';
src.AcquisitionFrameRateAbs = FrameRate;
src.DSPSubregionBottom = 1080;
src.DSPSubregionRight = 1920;
vid.ROIPosition = [0 0 1920 1080];
vid.LoggingMode = 'disk';
%%
srcBoson.FrameRate = '7.5000';
boson.LoggingMode = 'disk';
%%
filename=['C:\Records\MatlabRecords\RecordsDianaAndYuval\',datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.mp4'];
diskLogger = VideoWriter(filename, 'MPEG-4');
diskLogger.FrameRate = 10;
vid.DiskLogger = diskLogger;
%%
boson.DiskLogger = diskLogger;
%%

%% lidar setup 
lidar = velodynelidar('VLP16');

%% start recording
start(lidar)
start(vid);
preview(vid);
%%
start(boson);
preview(boson);
%%
msgbox('Recording..')

%% reading 500 frames from velodyne than stop
numFrames = RecordTime*10;
[frames,timestamps] = read(lidar,numFrames);

%% stop recording 
stop(vid);
stop(lidar);
stoppreview(vid);
%%
stoppreview(boson);
stop(boson);
%%
msgbox('Finished Recording. Saving Data..')
fps=1/mean(seconds(diff(timestamps)));

%% save data - video as frames, velodyne as point clouds


% create output folders
SavedVideo = VideoReader(filename);
frameIdx = 1;
if ~exist([WorkingDir ['images_' datestr(now,'dd-mmm-yyyy-HH-MM')]], 'dir')
    CameraFileName = [WorkingDir ['images_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    LidarFileName = [WorkingDir ['velodynePoints_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    BosonFileName = [WorkingDir ['Boson_' datestr(now,'dd-mmm-yyyy-HH-MM')]];
    mkdir(CameraFileName);
    mkdir(LidarFileName);
    mkdir(BosonFileName);
end
%save camera video as frames 
while hasFrame(SavedVideo)
   img = readFrame(SavedVideo);
   filename = [sprintf('%04d',frameIdx) '.jpg'];
   fullname = fullfile(CameraFileName,filename);
   imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
   frameIdx = frameIdx+1;
   if mod(frameIdx,50) == 0 && frameIdx ~= 0
        img = readFrame(SavedVideo); % dump mod 50 frame because of frame rate mismatch
   end
       
end

%saving lidar pcap as frames

if frameIdx < numFrames
    numFrames = frameIdx;
end

for pcapIdx = 1:numFrames
    frameName = [sprintf('%04d',pcapIdx) '.ply'];
    Path = [LidarFileName '\' frameName];
    pcwrite(frames(pcapIdx),Path);
    %pcshow(frames(pcapIdx))
    Path = [BosonFileName '\' frameName];

end

