lidar = velodynelidar('VLP16');

%preview(lidar)
%pause(5)
%closePreview(lidar)

% Start acquisition
start(lidar)

[frame,timestamp] = read(lidar,1)

numFrames = 300;
[frames,timestamps] = read(lidar,numFrames);

stop(lidar)
fps=1/mean(seconds(diff(timestamps)))
clear lidar
save C:\Records\MatlabRecords\lidardata.mat frames timestamps