clc;
clear all;
vid = videoinput('gige', 1, 'BayerRG8');
src = getselectedsource(vid);

%vid.FramesPerTrigger = 900;
vid.FramesPerTrigger = Inf;


src.AcquisitionFrameCount = 65535;

preview(vid);
src.BalanceWhiteAuto = 'Once';
%src.ExposureAuto = 'Continuous';
src.AcquisitionFrameRateAbs = 30.000100001;

vid.ROIPosition = [0 0 1920 1080];

%vid.LoggingMode = 'disk&memory';
vid.LoggingMode = 'disk';

%filename=['C:\Records\MatlabRecords\GigE_Cam\Test_',datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.mp4'];
filename=['C:\Records\MatlabRecords\camera_check\',datestr(now,'dd-mmm-yyyy-HH-MM-SS-FFF'),'.mp4'];

diskLogger = VideoWriter(filename, 'MPEG-4');

vid.DiskLogger = diskLogger;




start(vid);
clock;
%[~,ts]=getdata(vid,300);


%stoppreview(vid);

%stop(vid);