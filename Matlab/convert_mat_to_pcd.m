

files = dir('/home/user/Desktop/Diana/pointcloud/*')

files = dir('/home/user/Desktop/Diana/pointcloud/*.mat')

for f=1:numel(files)
    current_file = files(f);

    full_file_path = fullfile(current_file.folder, current_file.name);
    if isfile(full_file_path)
        data = load(full_file_path)
        
        [~,name,~] = fileparts(current_file.name)
        full_file_path_dst = fullfile('/home/user/Desktop/Diana/pointcloud/dst', ...
            [name '.ply'])
        pc = pointCloud(data.velodyne_points)
        pcwrite(pc,full_file_path_dst)
    end
end


%% 

files = dir('/home/user/Desktop/Diana/calibration_2021-12-26-12-19-12/pointcloud_calibration_2021-12-26-12-19-12/*')

for f=1:numel(files)
    current_file = files(f);

    full_file_path = fullfile(current_file.folder, current_file.name);
    full_file_path_dst = fullfile(current_file.folder, [current_file.name '.mat']);

    if isfile(full_file_path)
        movefile(full_file_path, full_file_path_dst)
    end
end


%%
ff

%%
pc = pcread('/home/user/Desktop/Diana/shahar/pc/1.ply')

figure, pcshow(pc)

%xyz = [-1.3 0.16 -0.3];
xyz = [0.96 -0.18 -0.017];

search_range = 2
roi = [xyz(1) - search_range, xyz(1) + search_range; xyz(2) - search_range, xyz(2) + search_range; xyz(3) - search_range, xyz(3) + search_range]
indices = findPointsInROI(pc,roi)

ptCloudB = select(pc,indices);

figure, pcshow(ptCloudB)


pcwrite(ptCloudB, '/home/user/Desktop/Diana/shahar/pc_trim/1.ply')

