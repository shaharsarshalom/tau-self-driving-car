import cameratransform as ct


def create_camera_sensor_model(camera_tilt_deg, heading_deg, camera_roll_deg, focal_mm, Frame_Width, Frame_Height,
                               Pixelsize_mm, pos_x_m=None,lens=None, projection=None):

    cam_orientation = ct.SpatialOrientation(tilt_deg=camera_tilt_deg, roll_deg=camera_roll_deg,
                                            heading_deg=heading_deg, pos_x_m=pos_x_m, pos_y_m=0)

    projection = None
    lens = None
    if projection is None:
        projection = ct.RectilinearProjection(focallength_mm=focal_mm,
                                             image_width_px=Frame_Width,
                                             image_height_px=Frame_Height,
                                             sensor_width_mm=Frame_Width * Pixelsize_mm,
                                             sensor_height_mm=Frame_Height * Pixelsize_mm)
    cam = ct.Camera(projection,
                    cam_orientation, lens=lens)

    return cam


imu_pitch = ?
imu_yaw
imu_roll
focal_mm =
frame_width= ?
frame_height


# initialize the camera
cam = create_camera_sensor_model(camera_tilt_deg=imu_pitch, heading_deg=imu_yaw, camera_roll_deg=imu_roll,
                                 focal_mm=focal_mm, Frame_Width=frame_width, Frame_Height=frame_height,
                                 Pixelsize_mm=pixel_size_mm, pos_x_m=current_camera_params['pos_x_m'])

cam.setGPSpos(latitude, longitude, current_camera_params['Height_Install'])


import scipy.io
camera_model_mat = scipy.io.loadmat('cameraParams_2022_02_10_large_board.mat')
type(camera_model_mat)
camera_model_mat.keys()
camera_model_mat['None']

camera_model_mat['__header__']



[V] FocalLength: [1.039641431856563e+03 1.036702725659338e+03]
PrincipalPoint: [9.549627381979604e+02 6.018702493765220e+02]
ImageSize: [1216 1936]
RadialDistortion: [-0.141957928615155 0.056841058684063]
TangentialDistortion: [0 0]
Skew: 0


# intrinsic camera parameters
f = 6.2    # in mm
sensor_size = (6.17, 4.55)    # in mm
image_size = (3264, 2448)    # in px

# initialize the camera
# cam = ct.Camera(ct.RectilinearProjection(focallength_mm=f,
#                                          sensor=sensor_size,
#                                          image=image_size),
#                ct.SpatialOrientation(elevation_m=10,
#                                      tilt_deg=45))



cam = ct.Camera(ct.RectilinearProjection(focallength_x_px=1.039641431856563e+03,
                                    focallength_y_px=1.036702725659338e+03,
                                         image_width_px=1936,image_height_px=1216),
               ct.SpatialOrientation(elevation_m=1.45+0.2,
                                     tilt_deg=90))

valid_switch_space = cam.spaceFromImage([1936/2, 1216 -100], Z=0)

















