clear all; 

%% Camera cablibration 
cameraCalibrator
camera_params.Intrinsics
camera_params.Intrinsics.IntrinsicMatrix

save('/home/user/Desktop/Diana/cameraParams_befor_car_test_2022_02_01', 'cameraParams_befor_car_test_2022_02_01')

save('/home/user/Desktop/Diana/cameraParams_2022_02_10_large_board', 'cameraParams_2022_02_10_large_board', 'estimationErrors_2022_02_10_large_board')

%%

pc_path = '/home/user/Desktop/Diana/pointcloud/dst'
images_path = '/home/user/Desktop/Diana/images'
camera_params_path = '/home/user/Desktop/Diana'
if 0 
    camera_params_path = fullfile(camera_params_path, 'camera_params.mat')
    %save(fullfile(imagePath, 'camera_params.mat'), 'cameraParams')
    d = load(camera_params_path);
    camera_params = d.cameraParams;

    squareSize = 20; % Square size of the checkerboard
else
    d = load('/home/user/Desktop/Diana/cameraParams_2022_02_10_large_board.mat');
    camera_params = d.cameraParams_2022_02_10_large_board  

    squareSize = 36; % Square size of the checkerboard
end 

%imagePath = fullfile(toolboxdir('lidar'), 'lidardata', 'lcc', 'vlp16', 'images');
%ptCloudPath = fullfile(toolboxdir('lidar'), 'lidardata', 'lcc', 'vlp16', 'pointCloud');
%cameraParamsPath = fullfile(imagePath, 'calibration.mat');

%intrinsic = load(cameraParamsPath); % Load camera intrinscs
imds = imageDatastore(images_path); % Load images using imageDatastore
pcds = fileDatastore(pc_path, 'ReadFcn', @pcread); % Loadr point cloud files

%%
if 1
    imds = imds.subset(1:10)
    pcds = pcds.subset(1:10)
    whos imageFileNames
    whos ptCloudFileNames
end 

%%
imageFileNames = imds.Files;
ptCloudFileNames = pcds.Files;



% Set random seed to generate reproducible results.
rng('default');

% Extract Checkerboard corners from the images
[imageCorners3d, checkerboardDimension, dataUsed] = ...
    estimateCheckerboardCorners3d(imageFileNames, camera_params, squareSize, 'ShowProgressBar', true);

size(imageCorners3d)

%% Project the detector plane to the image plane - this would be helpfull 
% to select the correct images 
close all;

% option 1 - using matlab build in function 
helperShowImageCorners(imageCorners3d, imageFileNames, camera_params);

% option 2 - using this code - show only the correct ones 
used_images_nums = find(dataUsed);
num_correct_imagse = numel(used_images_nums);
for i = 1:numel(used_images_nums)
    img_index = used_images_nums(i);

    boardCorners = imageCorners3d(:,:,i);
    Image = imread(imageFileNames{img_index});
    imPts = projectLidarPointsOnImage(boardCorners,camera_params,rigid3d());
    J = undistortImage(Image,camera_params);

    f = figure;
    imshow(J);
    hold on;
    plot(imPts(:,1),imPts(:,2),'.r','MarkerSize',30);
    hold on;
    title(sprintf('Detected Checkerboard Corners image index %d loop index %d', img_index, i));
    hold off;
    drawnow;
end

close all

%% Checkerboard Detection in Lidar

imageFileNames = imageFileNames(dataUsed); % Remove image files that are not used

% Filter point cloud files corresponding to the detected images
ptCloudFileNames = ptCloudFileNames(dataUsed);

%%
% Extract ROI from the detected image corners
roi = helperComputeROI(imageCorners3d, 1);
size(roi)

for i=1:numel(ptCloudFileNames)
    pc = pcread(ptCloudFileNames{i});
    % figure, pcshow(pc)

    pc_inside_index = pc.findPointsInROI(roi);
    pc_inside = pc.select(pc_inside_index);
    figure, pcshow(pc_inside); 
    hold on;

    [filepath,name,ext] = fileparts(ptCloudFileNames{i});
    title(sprintf('[pc index %d][pc path %s]', i, ptCloudFileNames{i}));
end

close all;

%%
[lidarCheckerboardPlanes, framesUsed, indices] = detectRectangularPlanePoints(...
    ptCloudFileNames, checkerboardDimension, 'DimensionTolerance', 0.07, ...
    'RemoveGround', false, 'ROI', roi, 'Verbose', 1);
if 0 
     [lidarCheckerboardPlanes, framesUsed, indices] = detectRectangularPlanePoints(...
        ptCloudFileNames, checkerboardDimension, 'RemoveGround', true);
    
    %Extract Checkerboard in lidar data
    [lidarCheckerboardPlanes, framesUsed, indices] = detectRectangularPlanePoints(...
        ptCloudFileNames, checkerboardDimension, 'RemoveGround', true, 'ROI', roi);
end 

imageCorners3d = imageCorners3d(:, :, framesUsed);
% Remove ptCloud files that are not used
ptCloudFileNames = ptCloudFileNames(framesUsed);
% Remove image files 
imageFileNames = imageFileNames(framesUsed);

%% To visualize the detected checkerboard,
helperShowCheckerboardPlanes(ptCloudFileNames, indices)

close all;

for i=1:numel(ptCloudFileNames)
    pc = pcread(ptCloudFileNames{i});
    % figure, pcshow(pc)

    pc_inside_index = pc.findPointsInROI(roi);
    pc_inside = pc.select(pc_inside_index);
    %figure, pcshow(pc_inside); 
    %figure, pcshow(lidarCheckerboardPlanes(i))
    figure, pcshowpair(pc_inside, lidarCheckerboardPlanes(i))

    hold on;

    [filepath,name,ext] = fileparts(ptCloudFileNames{i});
    title(sprintf('[pc index %d][pc path %s]', i, ptCloudFileNames{i}));
end

%% Calibrating Lidar and Camera

[lidar_to_camera_trans, errors] = estimateLidarCameraTransform(lidarCheckerboardPlanes, ...
    imageCorners3d, 'CameraIntrinsic', camera_params);

%% Disply the results - 
helperFuseLidarCamera(imageFileNames, ptCloudFileNames, indices,...
    camera_params, lidar_to_camera_trans);

% Error Visualization
helperShowError(errors)

%% Project the pc on the image 

imds = imageDatastore(images_path); % Load images using imageDatastore
pcds = fileDatastore(pc_path, 'ReadFcn', @pcread); % Loadr point cloud files

%for i=1:numel(imds.Files)
for i = 1:10
    current_img = imds.readimage(i);
    current_pc = pcread(pcds.Files{i});

    [imPts, lidar_to_img_indicies] = projectLidarPointsOnImage(current_pc,camera_params,lidar_to_camera_trans);
    size(imPts)
    size(lidar_to_img_indicies)
    
    % project the pc to the image plane 
    figure;
    imshow(current_img);
    hold on;
    plot(imPts(:,1),imPts(:,2),'.','Color','r');
    hold off;

    % display the input points 
    inside_pc = current_pc.select(lidar_to_img_indicies);
    figure, pcshow(inside_pc)

    figure(1)
    x = inside_pc.Location(:,1)
    y = inside_pc.Location(:,2)
    z = inside_pc.Location(:,3)
    stem3(x, y, z)
    grid on
    xv = linspace(min(x), max(x), 20);
    yv = linspace(min(y), max(y), 20);
    [X,Y] = meshgrid(xv, yv);
    Z = griddata(x,y,z,X,Y);
    figure(2)
    surf(X, Y, Z);
    grid on
    set(gca, 'ZLim',[0 100])
    shading interp
    axis equal 
    %surf(inside_pc.Location(:,1)', inside_pc.Location(:,2)', inside_pc.Location(:,3)');

    ptCloudOut = fuseCameraToLidar(current_img,current_pc,camera_params,lidar_to_camera_trans);
    figure; 
    pcshowpair(current_pc, ptCloudOut);

end

%% 
save('/home/user/Documents/tau-self-driving-car/lidar_to_camera_trans.mat', 'lidar_to_camera_trans')



