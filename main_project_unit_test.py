
if __name__ == '__main__':

    rosbag_file = ''

    # create rosbag utility file
    rosbag_obj = RosbagUtility(rosbag_file=rosbag_file)

    # create the detector and tracker object
    detector_tracker_obj = Yolo4DeepSort()

    depth_obj = DepthEstimator()

    dynamic_obj = DynamicVsStaticEstimator()

    # iterate over all the images in the dataset
    for data in rosbag_obj:

        # given an image, send it to the detector
        tracks = detector_tracker_obj.detect_and_track(frame=data.frame)

        # estimate depth using the velodny points
        tracks = depth_obj.estimate_depth(tracks,data)

        tracks = dynamic_obj.estimate_pose(tracks)

        # print or save to video



